<?php
use app\Models\State;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('laws', function (Blueprint $table) {
            $table->id();
            $table->foreignId('state_id')->references('id')->on('state');
            $table->string('bill_number', 100);
            $table->string('latest_stage', 4000);
            $table->date('intro_date');
            $table->string('bill_impact', 4000);
            $table->string('issues', 4000);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('laws');
    }
};
