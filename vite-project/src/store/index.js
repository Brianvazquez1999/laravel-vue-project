import { createStore } from "vuex";
import axiosClient from "../axios";

const store = createStore(
     {
        state: {
            user: {
                data:{},
                token: sessionStorage.getItem('TOKEN'),
            },
            states:[],
            laws:[]
        },
        getters: {},
        actions: {
            register({commit}, user) {
                return axiosClient.post("/register", user)
                .then(({data}) => {
                    commit('setUser', data);
                    return data;
                })
            },
            login({commit}, user) {
                return axiosClient.post("/login", user)
                .then(({data}) => {
                    commit('setUser', data);
                    return data;
                })
            },

            logout({commit}) {
                return axiosClient.post('/logout')
                .then(response => {
                    commit('logout')
                    return response
                })
            },

            getStates({commit}) {
                return axiosClient.get('/states')
                .then(({data}) => {
                    commit('setStates', data)
                    return data;
                })
            },
            getLaws({commit}) {
                return axiosClient.get('/laws')
                .then(({data}) => {
                    commit('setLaws', data)
                    return data
                })
            }
        },
        mutations : {
            setUser: (state, userData) => {
                state.user.token = userData.token;
                state.user.data = userData.user;
                sessionStorage.setItem('TOKEN', userData.token);
            },
            logout: (state) => {
                state.user.token = null;
                state.user.data = {};
                sessionStorage.removeItem('TOKEN');
            },
            setStates: (state, stateData) => {
                state.states = stateData
            },
            setLaws: (state, lawsData) => {
                state.laws = lawsData
            }
        },
        modules: {},
     }
)

export default store
