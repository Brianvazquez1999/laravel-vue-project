<?php

namespace App\Http\Controllers;

use App\Models\Laws;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLawsRequest;
use App\Http\Requests\UpdateLawsRequest;

class LawsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $laws = Laws::all();
        return response() -> json($laws);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreLawsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Laws $laws)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateLawsRequest $request, Laws $laws)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Laws $laws)
    {
        //
    }
}
